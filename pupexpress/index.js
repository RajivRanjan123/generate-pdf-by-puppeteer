
const puppeteer= require("puppeteer");
const express=require("express");
const app=express();

app.get("/pdf", async(req,res) =>{
    const url=req.query.target;
    const browser= await puppeteer.launch({
        headless: true
    });
    const webPage= await browser.newPage();
    await webPage.goto(url,{
        waitUntil: "networkidle0"
    });
    const pdf= await webPage.pdf({
        printBackground: true,
        format: "A4",
        margin: {
            top: "10px",
            bottom: "10px",
            left: "10px",
            right: "10px"
        }
    });
    await browser.close();

    res.contentType("application/pdf");
    res.send(pdf);

})

app.listen(3001, ()=>{
    console.log("server started..")
})
